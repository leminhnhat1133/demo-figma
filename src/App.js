import { Suspense } from "react";
import Header from "./components/Header";
import Loading from "./components/Loading/index";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import GlobalStyle from "./common/Theme/global-styles";
import { ROUTE_MAIN } from "./constants/routes";

function App() {
    return (
        <Router>
            <Header></Header>
            <Suspense fallback={<Loading/>}>
                <Switch>
                    {ROUTE_MAIN.length > 0 && ROUTE_MAIN.map((route,index)=>{
                        return (
                            <Route key={index} path={route.path} exact={route.exact} component={route.main}/>
                        );
                    })}
                </Switch>
            </Suspense>
            <GlobalStyle />
        </Router>
    );
}

export default App;
