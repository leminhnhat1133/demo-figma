import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { Provider } from 'react-redux';
import store from './store'
import { ThemeProvider } from 'styled-components';
import theme from './common/Theme'

ReactDOM.render(
    // <React.StrictMode>
    <Provider store={store}>
        <ThemeProvider theme={theme}>
            <App />
        </ThemeProvider>
    </Provider>
    // </React.StrictMode>
    ,
    document.getElementById('root')
);

reportWebVitals();
