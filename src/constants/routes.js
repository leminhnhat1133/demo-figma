import React from 'react';
import NotFound from '../components/NotFound';
import Profile from '../features/Profile/pages/Profile';
import TalentProfile from '../features/Profile/pages/TalentProfile';
import Learning from './../features/Courses/pages/Learning';
import Learnt from './../features/Courses/pages/Learnt';
import Favourites from './../features/Courses/pages/Favourites';
import Others from './../features/Courses/pages/Others';
import Information from './../features/Course/pages/Information';
import StudyRoute from './../features/Course/pages/StudyRoute';
import Docs from './../features/Course/pages/Docs';
import Videos from './../features/Course/pages/Videos';
import Challenges from './../features/Course/pages/Challenges';
import ListOfStudent from './../features/Course/pages/ListOfStudent';
import Schedules from './../features/Course/pages/Schedules';
const Home = React.lazy(()=> import("../features/Home"));
const ProfileFeature = React.lazy(()=> import("../features/Profile"));
const CoursesFeature = React.lazy(()=> import("../features/Courses"));
const CourseFeature = React.lazy(()=> import("../features/Course"));

export const ROUTE_MAIN = [
    {
        path:'/',
        exact:true,
        main:()=><Home/>
    },
    {
        path:'/profile',
        exact:false,
        main:()=><ProfileFeature/>
    },
    {
        path:'/courses',
        exact:false,
        main:()=><CoursesFeature/>
    },
    {
        path:'/course/:id',
        exact:false,
        main:()=><CourseFeature/>
    },
    {
        path:'',
        exact:false,
        main:()=><NotFound/>
    }
];

export const ROUTE_PROFILE = [
    {
        path:'/profile/',
        exact:true,
        main:()=><Profile/>
    },
    {
        path:'/profile/talent',
        exact:false,
        main:()=><TalentProfile/>
    },
    {
        path:'',
        exact:false,
        main:()=><NotFound/>
    }
]

export const ROUTE_COURSES = [
    {
        path:'/courses/',
        exact:true,
        main:()=><Learning/>
    },
    {
        path:'/courses/learnt',
        exact:false,
        main:()=><Learnt/>
    },
    {
        path:'/courses/favourites',
        exact:false,
        main:()=><Favourites/>
    },
    {
        path:'/courses/others',
        exact:false,
        main:()=><Others/>
    },
    {
        path:'',
        exact:false,
        main:()=><NotFound/>
    }
]

export const ROUTE_COURSE = [
    {
        path:'/course/:id/study-route',
        exact:true,
        main:()=><StudyRoute/>
    },
    {
        path:'/course/:id/detail/information',
        exact:true,
        main:()=><Information/>
    },
    {
        path:'/course/:id/documents/docs',
        exact:true,
        main:()=><Docs/>
    },
    {
        path:'/course/:id/documents/videos',
        exact:true,
        main:()=><Videos/>
    },
    {
        path:'/course/:id/challenges',
        exact:true,
        main:()=><Challenges/>
    },
    {
        path:'/course/:id/list-of-students',
        exact:true,
        main:()=><ListOfStudent/>
    },
    {
        path:'/course/:id/schedules',
        exact:true,
        main:()=><Schedules/>
    },
    {
        path:'',
        exact:false,
        main:()=><NotFound/>
    }
]