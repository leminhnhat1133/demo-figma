import DetailsOutlinedIcon from '@material-ui/icons/DetailsOutlined';
import MenuBookTwoToneIcon from '@material-ui/icons/MenuBookTwoTone';
import AccessTimeTwoToneIcon from '@material-ui/icons/AccessTimeTwoTone';
import PeopleAltOutlinedIcon from '@material-ui/icons/PeopleAltOutlined';
import DateRangeOutlinedIcon from '@material-ui/icons/DateRangeOutlined';
import FormatListBulletedIcon from '@material-ui/icons/FormatListBulleted';

export const MENU_MAIN = [
    {
        title:'My courses',
        to:'/courses',
        exact:false
    },
    {
        title:'Library',
        to:'/library',
        exact:false
    }
];

export const MENU_PROFILE = [
    {
        title:'Talent profile',
        to:'/profile/talent',
        exact:true
    },
    {
        title:'Profile',
        to:'/profile',
        exact:true
    }
]

export const MENU_COURSES = [
    {
        title:'Learning',
        to:'/courses',
        exact:true
    },
    {
        title:'Learnt',
        to:'/courses/learnt',
        exact:true
    },
    {
        title:'Favourites',
        to:'/courses/favourites',
        exact:true
    },
    {
        title:'Others',
        to:'/courses/others',
        exact:true
    }
]

export const MENU_COURSE = (id) => [
    {
        title:'Study route',
        to:`/course/${id}/study-route`,
        Icon: ()=><FormatListBulletedIcon/>,
        subItems:[   
        ]
    },
    {
        title:'Details of class',
        to:`/course/${id}/detail`,
        Icon: ()=><DetailsOutlinedIcon/>,
        subItems:[
            {
                title:'Information',
                to:`/course/${id}/detail/information`
            }
        ]
    },
    {
        title:'Documents',
        to:`/course/${id}/documents`,
        Icon: ()=><MenuBookTwoToneIcon/>,
        subItems:[
            {
                title:'Docs',
                to:`/course/${id}/documents/docs`
            },
            {
                title:'Video',
                to:`/course/${id}/documents/videos`
            }
        ]
    },
    {
        title:'Challenges',
        to:`/course/${id}/challenges`,
        Icon: ()=><AccessTimeTwoToneIcon/>,
        subItems:[
        ]
    },
    {
        title:'List of students',
        to:`/course/${id}/list-of-students`,
        Icon: ()=><PeopleAltOutlinedIcon/>,
        subItems:[
        ]
    },
    {
        title:'Schedules',
        to:`/course/${id}/schedules`,
        Icon: ()=><DateRangeOutlinedIcon/>,
        subItems:[
        ]
    }
]
