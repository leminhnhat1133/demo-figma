import { configureStore } from '@reduxjs/toolkit';
import appReducer from './appSlice';
import coursesReducer from './features/Courses/coursesSlice';
import courseReducer from './features/Course/courseSlice';

const rootReducer = {
    app: appReducer,
    courses: coursesReducer,
    course: courseReducer,
}  

const store = configureStore({
    reducer: rootReducer,
})

export default store;