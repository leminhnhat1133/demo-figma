const { createSlice } = require("@reduxjs/toolkit");

const app = createSlice({
    name: 'app',
    initialState: {
        isLogin: false
    },
    reducers:{
        login: (state)=>{
            state.isLogin = true;
        },
        logout: (state)=>{
            state.isLogin = false;
        }
    }
});

const {reducer,actions}=app;
export const { login,logout } = actions;
export default reducer;