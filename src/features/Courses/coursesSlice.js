import html from './../../assets/img/html.png';
import css from './../../assets/img/css.png';
import js from './../../assets/img/js.png';
import fe from './../../assets/img/front-end.png';
import unity from './../../assets/img/unity.png';
import be from './../../assets/img/back-end.png';
const { createSlice } = require("@reduxjs/toolkit");


const courses = createSlice({
    name: 'courses',
    initialState: {
        items:[
            {
                id:1,
                name:'CSS - FE21',
                image:css,
                desc: 'Short description for this class lorem please ipsum haha ipsor merolm.',
                teacher:{
                    id:1,
                    name:'Minh Nhật'
                },
                time:'05/09 - 15/09/2021'
            },
            {
                id:2,
                name:'HTML - FE22',
                image:html,
                desc: 'Short description for this class lorem please ipsum haha ipsor merolm.',
                teacher:{
                    id:1,
                    name:'Minh Nhật'
                },
                time:'05/09 - 15/09/2021'
            },
            {
                id:3,
                name:'JS - JSBC21',
                image:js,
                desc: 'Short description for this class lorem please ipsum haha ipsor merolm.',
                teacher:{
                    id:1,
                    name:'Minh Nhật'
                },
                time:'05/09 - 15/09/2021'
            },{
                id:4,
                name:'CSS - FE21',
                image:css,
                desc: 'Short description for this class lorem please ipsum haha ipsor merolm.',
                teacher:{
                    id:1,
                    name:'Minh Nhật'
                },
                time:'05/09 - 15/09/2021'
            },{
                id:5,
                name:'Front end chuyên nghiệp - FE21',
                image:fe,
                desc: 'Short description for this class lorem please ipsum haha ipsor merolm.',
                teacher:{
                    id:1,
                    name:'Minh Nhật'
                },
                time:'05/09 - 15/09/2022'
            },{
                id:6,
                name:'HTML - FE21',
                image:html,
                desc: 'Short description for this class lorem please ipsum haha ipsor merolm.',
                teacher:{
                    id:1,
                    name:'Minh Nhật'
                },
                time:'05/09 - 15/09/2021'
            },{
                id:7,
                name:'Front end chuyên nghiệp - FE22',
                image:fe,
                desc: 'Short description for this class lorem please ipsum haha ipsor merolm.',
                teacher:{
                    id:1,
                    name:'Minh Nhật'
                },
                time:'05/09 - 15/09/2022'
            },
            {
                id:8,
                name:'Back end chuyên nghiệp - BE22',
                image:be,
                desc: 'Short description for this class lorem please ipsum haha ipsor merolm.',
                teacher:{
                    id:1,
                    name:'Minh Nhật'
                },
                time:'05/09 - 15/09/2021'
            },
            {
                id:9,
                name:'Unity cơ bản - UBC21',
                image:unity,
                desc: 'Short description for this class lorem please ipsum haha ipsor merolm.',
                teacher:{
                    id:1,
                    name:'Minh Nhật'
                },
                time:'05/09 - 15/09/2021'
            }
        ]
    },
    reducers:{
        
    }
});

const {reducer,actions}=courses;
// export const { login,logout } = actions;
export default reducer;