import { Grid, makeStyles } from '@material-ui/core';
import React from 'react';
import { useSelector } from 'react-redux';
import CourseItem from '../../components/CourseItem';

const useStyles = makeStyles({
    wrapper:{
        width:'calc(100% - 200px)',
        margin:'0 auto',
        marginTop:'50px'
    }
})

const Learnt = () => {
    const classes = useStyles();

    const items = useSelector(state=>state.courses.items);

    return (
        <div className={classes.wrapper}>
            <Grid container spacing={10}>
                {
                    items.length && items.map((item,index)=>{
                        return (
                            <Grid key={index} item xs={4}>
                                <CourseItem item={item}/>
                            </Grid>
                        ) 
                    })
                }
            </Grid>
        </div>
    );
};

export default Learnt;