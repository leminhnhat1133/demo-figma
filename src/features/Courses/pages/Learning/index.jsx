import { Grid, makeStyles } from '@material-ui/core';
import { Pagination } from '@material-ui/lab';
import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import CourseItem from '../../components/CourseItem';

const useStyles = makeStyles({
    wrapper:{
        width:'calc(100% - 200px)',
        margin:'0 auto',
        marginTop:'50px',
    },
    pagination:{
        margin:'50px auto',
        display:'flex',
        justifyContent:'center'
    }
})

const Learning = () => {
    const classes = useStyles();

    const items = useSelector(state=>state.courses.items);
    console.log(items)
    
    const [pagination,setPagination] = useState({
        totalItem:items.length,
        currentPage:1,
        itemPerPage:3
    });
    const totalPage = Math.ceil(pagination.totalItem / pagination.itemPerPage);

    const handleChange = (event, value) => {
        setPagination({
            ...pagination,
            currentPage: value
        });
    }

    const { currentPage , itemPerPage } = pagination;
    const indexLastItem = currentPage * itemPerPage;
    const indexFirstItem = indexLastItem - itemPerPage; 
    const currentItem =  items.slice(indexFirstItem,indexLastItem);

    return (
        <div className={classes.wrapper}>
            <Grid container spacing={10}>
                {
                    currentItem.length && currentItem.map((item,index)=>{
                        return (
                            <Grid key={index} item xs={4}>
                                <CourseItem item={item}/>
                            </Grid>
                        ) 
                    })
                }
            </Grid>
            <div className={classes.pagination}>
                <Pagination count={totalPage} page={currentPage} onChange={handleChange} ></Pagination>
            </div>
        </div>
    );
};

export default Learning;