import { makeStyles } from '@material-ui/core';
import React from 'react';
import { Link } from 'react-router-dom';

const useStyles = makeStyles({
    root:{
        width:'100%',
        height:'300px',
        background: '#c4c4c4',

        display:'flex',
        flexDirection:'column'
    },
    img:{
        width:'100%',
        height:'200px',
    },
    title:{
        width:'100%',
        height:'40px',
        backgroundColor:'rgba(255, 255, 255, 0.8)',

        fontWeight:'600',
        lineHeight:'27px',
        fontSize:'20px',

        display:'flex',
        alignItems:'center',
        paddingLeft:'20px'
    },
    desc:{
        width:'100%',
        height:'50px',
        backgroundColor:'#fff',
        color:'#c4c4c4',
       
        fontSize:'16px',
        lineHeight:'20px',
        fontWeight:'400',
    },
    info:{
        backgroundColor:'#fff',

        display:'flex',
        justifyContent: 'space-between',
    },
    name:{
        color:'#AD1F59',
        fontWeight:'600',
        fontSize:'20px',
        lineHeight:'27px',
    },
    time:{
        color:'#173451',
        fontWeight:'500',
        fontSize:'18px',
        lineHeight:'27px',
    },
})

const CourseItem = (props) => {
    const classes = useStyles();

    const{item}=props;
    
    return (
        <div className={classes.root}>
            <Link className={classes.img} to={`/course/${item.id}/detail/information`}><img style={{width:'100%',height:'100%'}} src={item.image} /></Link>
            <Link className={classes.img} to={`/course/${item.id}/detail/information`}><div className={classes.title}>{item.name}</div></Link>
            <div className={classes.desc}>
                {item.desc}        
            </div>
            <div className={classes.info}>
                <Link to={`/teacher/${item.teacher.id}`}><div className={classes.name}>{item.teacher.name}</div></Link>
                <div className={classes.time}>{item.time}</div>
            </div>
        </div>
    );
};

export default CourseItem;