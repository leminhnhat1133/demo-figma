import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { MENU_COURSES } from '../../constants/menus';
import { ROUTE_COURSES } from '../../constants/routes';
import Nav from '../../components/Nav';

const CoursesFeature = () => {
    return (
        <div>
            <Nav items={MENU_COURSES}></Nav>
            <Switch>
                {ROUTE_COURSES.length > 0 && ROUTE_COURSES.map((route,index)=>{
                    return (
                        <Route key={index} path={route.path} exact={route.exact} component={route.main}/>
                    );
                })}
            </Switch>
        </div>
    );
};

export default CoursesFeature;