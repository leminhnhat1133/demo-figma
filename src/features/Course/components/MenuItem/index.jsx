import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Collapse from "@material-ui/core/Collapse";
import { Link, Route, useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import {toggleSidebar} from './../../courseSlice';

const useStyles = makeStyles(() => ({
    listItem: {
        display: 'flex',
        alignItems: 'center',

        width: '100%',
        height: '35px',
    },
    item:{
        display: 'flex',
        alignItems: 'center',
        width:'calc(100% + 20px)',
        height:'100%',
        background:'#fff',
        color: '#000',
        paddingLeft: '10px',
        paddingRight: '10px',
        marginRight:'10px',

        fontSize: '16px',
        lineHeight: '24px',
        fontWeight: '300',
    },
    text: {
        paddingLeft: '10px',
    },
    itemActive: {
        fontWeight: '500',
        background: 'rgba(255, 202, 56, 0.39)',
        color:'#FCBF16',
    },
    subListItem:{
        display: 'flex',
        alignItems: 'center',
        marginLeft:'50px',

        width: 'calc(100% - 50px)',
        height: '35px',
    },
    subItem:{
        background:'#fff',
        color: '#000',

        fontSize: '16px',
        lineHeight: '24px',
        fontWeight: '300',
    },
    subItemActive:{
        fontWeight:'600',
        color:'#AD1F59'
    }
}));

const SubItem = ({item})=>{
    const { to, title} = item;
    const classes = useStyles();
    return(
        <Route path={to}>
            {({ match })=>{
                return (
                    <li className={classes.subListItem}>
                        <Link to={to} className={`${classes.subItem} ${match && classes.subItemActive}`}>
                            <div className={classes.text}>{title}</div>
                        </Link>
                    </li>
                )
            }}
        </Route>
    )
}

const MenuItem = ({item}) => {
    const classes = useStyles();
    
    const show = useSelector(state => state.course.showSidebar);

    const dispatch = useDispatch();

    const history = useHistory();
    const path = history.location.pathname;
    const {title , to , subItems,Icon} = item

    const [open, setOpen] = React.useState(path.includes(to));

    const handleClick = () => {
        setOpen(!open);
    };

    const handleClickShow = () =>{
        setOpen(!open);
        dispatch(toggleSidebar());
    }

    return(
        <Route path={to}>
            {({ match })=>{
                return (
                    <>
                        {
                        subItems.length === 0 ? 
                            <li className={`${classes.listItem}`}>
                                <Link className={`${classes.item} ${match && classes.itemActive}`} to={to}>
                                    <Icon/>
                                    {show &&<div className={classes.text}>{title}</div>}
                                </Link>
                            </li> 
                        : 
                        <>
                            <li className={`${classes.listItem}`} onClick={show ? handleClick : handleClickShow}>
                                <div className={`${classes.item} ${match && classes.itemActive}`}>                                   
                                    <Icon/>
                                    {show &&<div className={classes.text}>{title}</div>}
                                </div>
                            </li>
                            <Collapse in={show&&open} timeout="auto" unmountOnExit>
                                <ul>
                                    {
                                        subItems.map((subItem, index)=>{
                                            return(
                                                <SubItem key={index} item={subItem}/>
                                            );
                                        })
                                    }
                                </ul>
                            </Collapse>
                        </>
                        }
                    </>
                );
            }}
        </Route>
    );
}

export default MenuItem;