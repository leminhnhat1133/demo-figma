import {  makeStyles } from '@material-ui/core';
import React from 'react';
import { MENU_COURSE } from '../../../../constants/menus';
import { useDispatch, useSelector } from 'react-redux';
import MenuItem from '../MenuItem';
import ArrowLeftOutlinedIcon from '@material-ui/icons/ArrowLeftOutlined';
import ArrowRightOutlinedIcon from '@material-ui/icons/ArrowRightOutlined';
import { toggleSidebar } from '../../courseSlice';

const useStyles = makeStyles({
    root:{
        width: 'calc(100% - 20px)',
        height:'calc(100vw - 955px)',
        paddingTop:'10px',
        paddingRight:'20px',
        borderRight:'2px solid #FCBF16',

        position:'relative',

        display:'flex',
        flexDirection:'column'
    },
    img:{
        width:'100%',
        height:'120px',

    },
    classCode:{
        fontSize:'14px',
        lineHeight:'21px',
        fontWeight:'400',

        marginTop:'5px'

    },
    line:{
        width:'100%',
        color:'#c4c4c4',
    },
    menu:{
        width:'100%'
    },
    btnHide:{
        position:'absolute',
        bottom:'20px',
        right:'20px',
        padding:'20px',

        cursor:'pointer'
    }
})

const Sidebar = ({item}) => {
    const classes = useStyles();

    const show = useSelector(state => state.course.showSidebar);
    const dispatch = useDispatch();
    
    const menuItems = MENU_COURSE(item.id);
    return (
        <div className={classes.root}>
            {
                show &&
                <>
                    <img src={item.image} className={classes.img}></img>
                    <div className={classes.classCode}>Class name: {item.name}</div>
                    <hr className={classes.line}/>
                </>
            }
            <ul className={classes.menu}>
                {
                    menuItems.length && menuItems.map((item,index)=>{
                        return (
                            <MenuItem key={index} item={item}/>
                        )
                    })
                }
            </ul>
            {
                show && 
                <div className={classes.btnHide} onClick={()=>dispatch(toggleSidebar())}>
                    <ArrowLeftOutlinedIcon fontSize="large" />
                </div>
            }
        </div>
    );
};

export default Sidebar;