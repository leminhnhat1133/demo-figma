import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { ROUTE_COURSE } from '../../constants/routes';
import Nav from '../../components/Nav';
import { Grid, makeStyles } from '@material-ui/core';
import Sidebar from './components/Sidebar';
import { useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';

const useStyles = makeStyles({
    wrapper:{
        width: 'calc(100% - 200px)',
        margin:'0 auto'
    }
})

const CourseFeature = () => {
    const classes = useStyles();

    const param = useParams();
    const { id } = param;
    const course = useSelector(state => state.courses.items.find(x=>x.id === Number(id)));

    const showSidebar = useSelector(state => state.course.showSidebar);

    return (
        <div>
            <Nav titleMode={true} title={course.name} subTitle="My course"></Nav>
            <div className={classes.wrapper}>
                <Grid container>
                    <Grid style={!showSidebar ? {maxWidth: '4%'} : {}} item xs={2}>
                        <Sidebar item={course}/>
                    </Grid>
                    <Grid item xs={10}>
                    <Switch>
                        {ROUTE_COURSE.length > 0 && ROUTE_COURSE.map((route,index)=>{
                            return (
                                <Route key={index} path={route.path} exact={route.exact} component={route.main}/>
                            );
                        })}
                    </Switch>
                    </Grid>
                </Grid>
            </div>
        </div>
    );
};

export default CourseFeature;