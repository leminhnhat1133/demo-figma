import { Grid, makeStyles } from '@material-ui/core';
import React from 'react';

const useStyles= makeStyles({
    root:{
        width: 'calc(100% - 20px)',
        paddingTop:'10px',
        paddingLeft:'20px',

        display:'flex',
        flexDirection:'column'
    },
    title:{
        fontSize: '20px',
        fontWeight: '600',
        lineHeight: '30px',
        color: '#AD1F59',
    },
    row:{
        paddingTop:'15px'
    },
    label:{
        fontSize:'16px',
        lineHeight:'24px',
        fontWeight:'600',

        color:'#000'
    },
    field:{
        width:'100%',
        borderBottom: '1px solid #C4C4C4',

        fontSize:'18px',
        lineHeight:'27px',
        fontWeight:'400',
    }
})

const Information = () => {
    const classes = useStyles();
    return (
        <div className={classes.root}>
            <div className={classes.title}>Information</div>
            <Grid className={classes.row} container spacing={4}>
                <Grid item xs={4}>
                    <label className={classes.label}>Teacher’s name</label>
                    <div className={classes.field}>Minh Nhật</div>
                </Grid>
                <Grid container item xs={4} >
                    <Grid item xs={12}>
                        <label className={classes.label}>Duration time</label>
                    </Grid>
                    <Grid item xs={6} style={{paddingRight:'8px'}}>
                        <div className={classes.field}>12/02/2021</div>
                    </Grid>
                    <Grid item xs={6} style={{paddingLeft:'8px'}}>
                        <div className={classes.field}>12/06/2021</div>
                    </Grid>
                </Grid>
                <Grid item xs={4}>
                    <label className={classes.label}>Number of students</label>
                    <div className={classes.field}>200</div>
                </Grid>
                <Grid item xs={8}>
                    <label className={classes.label}>Description</label>
                    <div className={classes.field}>
                        Description here lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                        Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.
                    </div>
                </Grid>
            </Grid>
            
        </div>
    );
};

export default Information;