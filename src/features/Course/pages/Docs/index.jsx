import { FormControl, InputAdornment, InputLabel, makeStyles, MenuItem, OutlinedInput, Select, TextField } from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import React from 'react';

const useStyles= makeStyles({
    root:{
        width: 'calc(100% - 20px)',
        paddingTop:'10px',
        paddingLeft:'20px',

        display:'flex',
        flexDirection:'column'
    },
    title:{
        fontSize: '20px',
        fontWeight: '600',
        lineHeight: '30px',
        color: '#AD1F59',
    },
    row:{
        display:'flex',
        paddingTop:'15px'
    },
    search:{
        padding:'0 5px',
        flexGrow: 1
    },
    filter:{
        padding:'0 5px',
        width:'200px'
    },
    button:{
        marginLeft:'5px',
        padding:'0 5px',
        width:'120px',

        background: '#173451',
        color:'#fff',
        border:'none',
        borderRadius:'3px',

        fontSize:'14px',
        fontWeight:'600',
        lineHeight:'21px'
    }
})

const Docs = () => {
    const classes = useStyles();
    return (
        <div className={classes.root}>
            <div className={classes.title}>Docs</div>
            <div className={classes.row}>
                <div className={classes.search}>
                    <OutlinedInput
                        style={{height:'40px'}}
                        fullWidth
                        placeholder="Find your documents..."
                        endAdornment={<SearchIcon/>}
                    />
                </div>
                <div className={classes.filter}>
                    <FormControl variant="outlined" fullWidth>
                        <Select style={{height:'40px'}} fullWidth defaultValue={1}>
                            <MenuItem value={1}>All documents</MenuItem>
                            <MenuItem value={2}>Front End</MenuItem>
                            <MenuItem value={3}>Back End</MenuItem>
                        </Select>
                    </FormControl>
                </div>
                <button className={classes.button}>
                    Upload
                </button>
            </div>
        </div>
    );
};

export default Docs;