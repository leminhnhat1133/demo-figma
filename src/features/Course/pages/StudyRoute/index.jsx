import { makeStyles } from '@material-ui/core';
import React from 'react';

const useStyles= makeStyles({
    root:{
        width: 'calc(100% - 20px)',
        paddingTop:'10px',
        paddingLeft:'20px',

        display:'flex',
        flexDirection:'column'
    },
    title:{
        fontSize: '20px',
        fontWeight: '600',
        lineHeight: '30px',
        color: '#AD1F59',
    }
})

const StudyRoute = () => {
    const classes = useStyles();
    return (
        <div className={classes.root}>
            <div className={classes.title}>Study Route</div>
        </div>
    );
};

export default StudyRoute;