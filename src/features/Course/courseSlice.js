const { createSlice } = require("@reduxjs/toolkit");

const course = createSlice({
    name: 'course',
    initialState: {
        showSidebar: true,
    },
    reducers:{
        toggleSidebar: (state)=>{
            state.showSidebar = !state.showSidebar;
        },
    }
});

const {reducer,actions}=course;
export const { toggleSidebar } = actions;
export default reducer;