import { Container, FormControl, Grid, Input, InputLabel, makeStyles } from '@material-ui/core';
import React from 'react';

const useStyles = makeStyles({
    root:{
        marginTop:'50px'
    },
    title:{
        color:'#FCBF16',
        fontSize: '22px',
        lineHeight:'33px',
        fontWeight:'600'
    },
    label:{
        fontWeight:'bold'
    }
})

const TalentProfile = () => {
    const classes = useStyles();
    return (
        <Container className={classes.root}>
            <Grid container spacing={10}>
                <Grid item xs={4}>
                    <span className={classes.title}>Your projects</span>
                    <FormControl fullWidth>
                        <InputLabel className={classes.label}>Project 1</InputLabel>
                        <Input defaultValue="https://twon.asia"/>
                    </FormControl>
                </Grid>
                <Grid item xs={4}>
                    <span className={classes.title}>Your articles</span>
                    <FormControl fullWidth>
                        <InputLabel className={classes.label}>Articles 1</InputLabel>
                        <Input defaultValue="fb.com/post/121412434565789999999987654"/>
                    </FormControl>
                </Grid>
                <Grid item xs={4}>
                    <span className={classes.title}>Your social activities</span>
                    <FormControl fullWidth>
                        <InputLabel className={classes.label}>Activity 1</InputLabel>
                        <Input defaultValue="https://gibhub.com/minhnhat0509/asdasfsaf12435"/>
                    </FormControl>
                </Grid>
            </Grid>
        </Container>
    );
};

export default TalentProfile;