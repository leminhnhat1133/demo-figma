import { Avatar, Container, FormControl, Grid, IconButton, Input, InputAdornment, InputLabel, TextField } from '@material-ui/core';
import { Visibility, VisibilityOff } from '@material-ui/icons';
import { makeStyles } from '@material-ui/styles';
import React, { useState } from 'react';
import avt from './../../../../assets/img/nhat.jpg';

const useStyles = makeStyles({
    root:{
        display:'flex',
        justifyContent: 'space-between',
        alignItems:'flex-start'
    },
    avatarItem:{
        marginTop: '50px',
        flexGrow:2,
        display:'flex',
        justifyContent:'center',
        alignItems:'center'
    },
    item:{
        marginTop: '50px',
        flexGrow:10
    },
    avatar:{
        width: '200px',
        height: '200px'
    },
    title:{
        color:'#FCBF16',
        fontSize: '22px',
        lineHeight:'33px',
        fontWeight:'600'
    },
    row:{
        marginBottom: '20px',
    },
    label:{
        fontWeight:'bold'
    },
    btn:{
        width:'200px',
        height:'50px',
        float:'right',

        border: 'none',
     
        background:'#C4C4C4',
        color:'#fff',
        fontWeight:'600',
        fontSize:'18px',
        lineHeight:'27px',
    },
    btnActive:{
        background: '#FCBF16',
    }
})

const Profile = () => {
    const [showPassword,setShowPassword] = useState(false);
    const [editForm,setEditForm] = useState(false);
    const classes = useStyles();
    return (
        <div className={classes.root}>
            <div className={classes.avatarItem}>
                <Avatar src={avt} className={classes.avatar}></Avatar>
            </div>
            <div className={classes.item}>
                <Grid container>
                    <Grid item xs={6}>
                        <span className={classes.title}>Your basic infomations</span>
                        <Grid className={classes.row} container>
                            <Grid item xs={4}>
                                <FormControl >
                                    <InputLabel className={classes.label}>First Name</InputLabel>
                                    <Input disabled={!editForm} defaultValue="Nhật"/>
                                </FormControl>
                            </Grid>
                            <Grid item xs={7}>
                                <FormControl fullWidth>
                                    <InputLabel className={classes.label}>Last Name</InputLabel>
                                    <Input disabled={!editForm} defaultValue="Lê Minh"/>
                                </FormControl>
                            </Grid>
                            <Grid item xs={1}/>
                        </Grid>
                        <Grid className={classes.row} container>
                            <Grid item xs={11}>
                                <FormControl fullWidth>
                                    <InputLabel className={classes.label}>Your date of birth</InputLabel>
                                    <Input disabled={!editForm} type="date" defaultValue="2000-09-05"/>
                                </FormControl>
                            </Grid>
                            <Grid item xs={1}/>
                        </Grid>
                        <Grid className={classes.row} container>
                            <Grid item xs={11}>
                                    <FormControl fullWidth>
                                        <InputLabel className={classes.label}>Your class</InputLabel>
                                        <Input disabled={!editForm} defaultValue="FE16-ReactJS"/>
                                    </FormControl>
                                </Grid>
                                <Grid item xs={1}/>
                            </Grid>
                    </Grid>
                    <Grid item xs={6}>
                        <span className={classes.title}>Your account settings</span>
                        <Grid className={classes.row} container>
                            <Grid item xs={11}>
                                <FormControl fullWidth>
                                    <InputLabel className={classes.label}>Your email address</InputLabel>
                                    <Input disabled={!editForm} type="email" defaultValue="leminhnhat1133@gmail.com"/>
                                </FormControl>
                            </Grid>
                            <Grid item xs={1}/>
                        </Grid>
                        <Grid className={classes.row} container>
                            <Grid item xs={11}>
                                <FormControl fullWidth>
                                    <InputLabel className={classes.label}>Your phone number</InputLabel>
                                    <Input disabled={!editForm} defaultValue="0704917152"/>
                                </FormControl>
                            </Grid>
                            <Grid item xs={1}/>
                        </Grid>
                        <Grid className={classes.row} container>
                            <Grid item xs={11}>
                                <FormControl fullWidth>
                                    <InputLabel className={classes.label}>Password</InputLabel>
                                    <Input
                                        disabled={!editForm}
                                        type={showPassword ? 'text' : 'password'}
                                        defaultValue="minhnhat0509"
                                        endAdornment={
                                        <InputAdornment position="end">
                                            <IconButton
                                            disabled={!editForm}
                                            aria-label="toggle password visibility"
                                            onClick={()=>setShowPassword(!showPassword)}
                                            >
                                            {showPassword ? <Visibility /> : <VisibilityOff />}
                                            </IconButton>
                                        </InputAdornment>
                                        }
                                    />
                                </FormControl>
                            </Grid>
                            <Grid item xs={1}/>
                        </Grid>
                        {
                            editForm &&
                            <Grid className={classes.row} container>
                                <Grid item xs={11}>
                                    <FormControl fullWidth>
                                        <InputLabel className={classes.label}>Retype your new password</InputLabel>
                                        <Input
                                            disabled={!editForm}
                                            type={showPassword ? 'text' : 'password'}
                                            defaultValue="minhnhat0509"
                                        />
                                    </FormControl>
                                </Grid>
                                <Grid item xs={1}/>
                            </Grid>
                        }
                        <Grid className={classes.row} container>
                            <Grid item xs={11}>
                                <button 
                                    className={`${classes.btn} ${editForm && classes.btnActive}`} 
                                    onClick={()=>setEditForm(!editForm)}
                                >
                                {editForm ? 'Save changes' : 'Edit Profile'}
                                </button>
                            </Grid>
                            <Grid item xs={1}/>
                        </Grid>
                    </Grid>
                </Grid>
            </div>
        </div>
    );
};

export default Profile;