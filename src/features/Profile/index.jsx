import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { MENU_PROFILE } from '../../constants/menus';
import { ROUTE_PROFILE } from '../../constants/routes';
import Nav from '../../components/Nav';

const ProfileFeature = () => {
    return (
        <div>
            <Nav items={MENU_PROFILE} start="right"></Nav>
            <Switch>
                {ROUTE_PROFILE.length > 0 && ROUTE_PROFILE.map((route,index)=>{
                    return (
                        <Route key={index} path={route.path} exact={route.exact} component={route.main}/>
                    );
                })}
            </Switch>
        </div>
    );
};

export default ProfileFeature;