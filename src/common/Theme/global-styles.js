import { createGlobalStyle } from "styled-components";

const GlobalStyle = createGlobalStyle`
  a{
    text-decoration: none;
    color: #000;
  };
  ul{
    padding:0;
    margin:0;
    list-style: none;
  }
`;
export default GlobalStyle;
