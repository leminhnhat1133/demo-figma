import { createMuiTheme, responsiveFontSizes } from "@material-ui/core/styles";

let theme = createMuiTheme({
  color: {
    edit: "#F0AD4E",
    delete: "#D9534F",
    add: "#28a745"
  }
});
theme = responsiveFontSizes(theme);
export default theme;