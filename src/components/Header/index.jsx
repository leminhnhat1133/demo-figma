import React, { useRef, useState } from 'react';
import logo from './../../assets/img/kmin_logo_02.png';
import avt from './../../assets/img/nhat.jpg'
import { makeStyles } from '@material-ui/styles';
import { useDispatch, useSelector } from 'react-redux';
import { login,logout } from '../../appSlice';
import { Avatar, Badge, Button, ClickAwayListener, Grid, Grow, MenuItem, MenuList, Paper, Popper } from '@material-ui/core';
import HelpOutlineIcon from '@material-ui/icons/HelpOutline';
import NotificationsNoneIcon from '@material-ui/icons/NotificationsNone';
import { Link, useHistory } from 'react-router-dom';
import { MENU_MAIN } from '../../constants/menus';

const useStyles = makeStyles({
    root:{
        width: '100%',
        height: '80px',

        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',

        background: '#FFFFFF'
    },
    item:{
        margin: '10px 100px',
        display: 'flex',
        alignItems: 'center',
    },
    logo:{
        height: '55px'
    },
    link:{
        color: '#000',
        marginLeft: '18px'
    },
    button:{
        marginLeft: '18px',
        height: '32px',
        width: '112px',

        background: '#fff',
        
        '&:hover': {
            cursor: 'pointer'   
        }
    },
    buttonPrimary:{
        background: '#FCBF16',
        border: '#FCBF16'
    },
    buttonIcon:{
        marginLeft: '18px',
        background: '#fff',
        '&:hover': {
            cursor: 'pointer'   
        }
    },
    avatarLarge:{
        width: '50px',
        height: '50px'
    }
});

const Header = () => {
    const classes = useStyles();
    
    const isLogin = useSelector(state => state.app.isLogin);
    const dispatch = useDispatch();
    const history = useHistory();

    //Handel Menu Avatar DropDown
    const [open, setOpen] = useState(false);
    const anchorRef = useRef(null);

    const handleLogout = ()=>{
        setOpen(false);
        history.push('/');
        dispatch(logout());
    }
    
    return (
        <div className={classes.root}>
            <div className={classes.item}>
                <Link to="/"><img className={classes.logo} src={logo}></img></Link>
                {
                    isLogin &&
                    <>
                        {
                            MENU_MAIN.length && MENU_MAIN.map((item,index)=>{
                                return <Link key={index} className={classes.link} to={item.to}>{item.title}</Link>
                            })
                        }
                    </>
                }
            </div>
            {!isLogin ? 
            <div className={classes.item}>
                <button className={classes.button} onClick={()=>dispatch(login())} >Login</button>
                <button className={`${classes.button} ${classes.buttonPrimary}`}>Sign Up</button>
                <HelpOutlineIcon className={classes.buttonIcon}></HelpOutlineIcon>
            </div> : 
            <div className={classes.item}>
                <Button
                    ref={anchorRef}
                    aria-controls={open ? 'menu-list-grow' : undefined}
                    aria-haspopup="true"
                    onClick={()=>setOpen((prevOpen) => !prevOpen)}
                >
                    <Avatar src={avt} className={classes.avatarLarge}></Avatar>
                </Button>
                {/* Menu Avatar*/}
                <Popper open={open} anchorEl={anchorRef.current} role={undefined} transition disablePortal>
                {({ TransitionProps, placement }) => (
                    <Grow
                    {...TransitionProps}
                    style={{ transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom' }}
                    >
                    <Paper>
                        <ClickAwayListener onClickAway={()=>setOpen(false)}>
                        <MenuList autoFocusItem={open} id="menu-list-grow">
                            <Link to="/profile"><MenuItem onClick={()=>setOpen(false)}>Profile</MenuItem></Link>
                            <MenuItem onClick={handleLogout}>Logout</MenuItem>
                        </MenuList>
                        </ClickAwayListener>
                    </Paper>
                    </Grow>
                )}
                </Popper>
                {/* End Menu */}
                <Badge badgeContent={4} color="secondary">
                    <NotificationsNoneIcon className={classes.buttonIcon}></NotificationsNoneIcon>
                </Badge>
                <HelpOutlineIcon className={classes.buttonIcon}></HelpOutlineIcon>
            </div>}
      </div>
    );
};

export default Header;