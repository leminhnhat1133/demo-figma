import { makeStyles } from '@material-ui/core';
import React from 'react';
import { Link, Route } from 'react-router-dom';

const useStyles = makeStyles({
    root:{
        width:'100%',
        height:'50px',
        backgroundColor:'#FCBF16',

        display:'flex',
        alignItems:'center',
        flexDirection:'row'
    },
    startRight:{
        flexDirection: 'row-reverse'
    },
    menuGroup:{
        margin: '0 100px'
    },
    menuItem:{
        padding: '15px 5px 10px 5px',
        color: '#fff',
        marginRight:'20px'
    },
    menuItemActive:{
        padding: '15px 5px 10px 5px',
        color: '#fff',
        fontWeight: 'bold',
        marginRight:'20px',
        borderBottom: '#fff 5px solid'
    },
    titleGroup:{
        fontSize:'16px',
        lineHeight:'24px',

        display:'flex',
        margin: '0 100px'
    },
    subTitle:{
        color:'rgba(255,255,255,0.8)',
        fontWeight:'400',
        paddingRight:'10px', 
    },
    title:{
        fontWeight:'600',
        color:'#fff'
    }
})

const CustomLink = ({ title,to , exact }) => {
    const classes = useStyles();
    return(
        <Route path={to} exact={exact}>
            {({ match })=>{
                return (
                    <Link to={to} className={match ? classes.menuItemActive : classes.menuItem}>{title}</Link>
                );
            }}
        </Route>
    );
}

const Nav = (props) => {
    const classes = useStyles();
    const {items = [],start,titleMode = false, title='', subTitle}=props;
    return (
        <div className={`${classes.root} ${start === 'right' && classes.startRight}`}>
            { 
                !titleMode ? 
                    <div className={classes.menuGroup}>
                    {
                        items.length && items.map((item,index)=>{
                            return <CustomLink key={index} title={item.title} to={item.to} exact={item.exact}/>
                        })
                    }
                    </div> 
                : 
                <div className={classes.titleGroup}>
                    {
                        subTitle && 
                        <>
                            <div className={classes.subTitle}>{subTitle}</div>
                            <div className={classes.subTitle}>|</div>
                        </>
                    }
                    <div className={classes.title}>{title}</div>
                </div>
            }
        </div>
    );
};

export default Nav;