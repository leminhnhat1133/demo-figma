import { CircularProgress } from '@material-ui/core';
import React from 'react';

function Loading(props) {
    return (
        <div>
            <CircularProgress/>
            Loading...
        </div>
    );
}

export default Loading;